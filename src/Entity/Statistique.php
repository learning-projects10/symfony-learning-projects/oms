<?php

namespace App\Entity;

use App\Repository\StatistiqueRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use DateTime;

/**
 * @ORM\Entity(repositoryClass=StatistiqueRepository::class)
 */
class Statistique
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Virus::class, inversedBy="statistiques")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotNull(message="Le virus ne peut pas être null.")
     */
    private $virus;

    /**
     * @ORM\ManyToOne(targetEntity=Pays::class, inversedBy="statistiques")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotNull(message="Le pays ne peut pas être null.")
     */
    private $pays;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Type("integer")
     * @Assert\NotBlank(message="Le nombre de contaminé ne peut pas être vide.")
     * @Assert\Length(
     *      min = 0,
     *      minMessage = "Le nombre de contaminé ne peut pas être plus petit que {{ min }}")
     */
    private $contaminated;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Type("integer")
     * @Assert\NotBlank(message="Le nombre de guéri ne peut pas être vide.")
     * @Assert\Length(
     *      min = 0,
     *      minMessage = "Le nombre de guéri ne peut pas être plus petit que {{ min }}")
     */
    private $recovered;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Type("integer")
     * @Assert\NotBlank(message="Le nombre de zombie ne peut pas être vide.")
     * @Assert\Length(
     *      min = 0,
     *      minMessage = "Le nombre de zombie ne peut pas être plus petit que {{ min }}")
     */
    private $zombie;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getVirus(): ?Virus
    {
        return $this->virus;
    }

    public function setVirus(?Virus $virus): self
    {
        $this->virus = $virus;

        return $this;
    }

    public function getPays(): ?Pays
    {
        return $this->pays;
    }

    public function setPays(?Pays $pays): self
    {
        $this->pays = $pays;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getContaminated(): ?int
    {
        return $this->contaminated;
    }

    public function setContaminated(int $contaminated): self
    {
        $this->contaminated = $contaminated;

        return $this;
    }

    public function getRecovered(): ?int
    {
        return $this->recovered;
    }

    public function setRecovered(int $recovered): self
    {
        $this->recovered = $recovered;

        return $this;
    }

    public function getZombie(): ?int
    {
        return $this->zombie;
    }

    public function setZombie(int $zombie): self
    {
        $this->zombie = $zombie;

        return $this;
    }
}
