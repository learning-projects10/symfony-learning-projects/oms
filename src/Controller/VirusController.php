<?php

namespace App\Controller;

use App\Entity\Virus;
use App\Form\VirusType;
use App\Repository\VirusRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/virus")
 * @IsGranted("ROLE_USER")
 */
class VirusController extends AbstractController
{
    /**
     * @Route("/", name="virus_index", methods={"GET"})
     */
    public function index(VirusRepository $virusRepository): Response
    {
        return $this->render('virus/index.html.twig', [
            'virus' => $virusRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="virus_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $virus = new Virus();
        $form = $this->createForm(VirusType::class, $virus);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($virus);
            $entityManager->flush();

            return $this->redirectToRoute('virus_index');
        }

        return $this->render('virus/new.html.twig', [
            'virus' => $virus,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="virus_show", methods={"GET"})
     */
    public function show(Virus $virus): Response
    {
        return $this->render('virus/show.html.twig', [
            'virus' => $virus,
        ]);
    }

}
